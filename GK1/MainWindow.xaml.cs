﻿using System.Windows;

namespace GK1
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        private void ClearClick(object sender, RoutedEventArgs e)
        {
            drawingCanvas.ClearCanvas();
        }

        private void LoadClick(object sender, RoutedEventArgs e)
        {
            drawingCanvas.ImportPolygon();
        }

        private void SaveClick(object sender, RoutedEventArgs e)
        {
            drawingCanvas.ExportPolygon();
        }
    }
}
