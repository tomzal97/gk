﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace GK1.Models
{
    class VertexInfo
    {
        public Point Position { get; set; }

        public bool IsRestricted { get; set; }

    }
}
