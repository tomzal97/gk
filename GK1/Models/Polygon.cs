﻿using GK1.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;

namespace GK1.Models
{
    public class Polygon
    {
        public bool IsClosed { get; set; }

        public DrawingCanvas ParentCanvas { get; private set; }
        public List<Vertex> Vertices { get; set; } = new List<Vertex>();
        public List<LineControl> Lines { get; set; } = new List<LineControl>();

        public Polygon(DrawingCanvas parent)
        {
            ParentCanvas = parent;
        }

        public List<Vertex> GetVertices()
        {
            return Vertices;
        }

        public List<LineControl> GetLines()
        {
            return Lines;
        }

        public void AddVertex(Vertex vertex)
        {
            Vertices.Add(vertex);
            if (Vertices.Count > 1)
            {
                var v1 = Vertices[Vertices.Count - 2];
                var v2 = vertex;
                Lines.Add(new LineControl(this, v1, v2));
            }
            Draw();
        }

        public void RemoveVertex(Vertex vertex)
        {
            if (Vertices.Count <= 3 && IsClosed)
                return;

            int ind = Vertices.IndexOf(vertex);

            var l1 = Lines.FirstOrDefault(l => l.V2 == vertex);
            var l2 = Lines.FirstOrDefault(l => l.V1 == vertex);

            if (vertex.IsRestricted)
                vertex.SetRestriction(LineRestriction.None);
            if (l1 != null && l1.V1.IsRestricted)
                l1.V1.SetRestriction(LineRestriction.None);
            if (l2 != null && l2.V2.IsRestricted)
                l2.V2.SetRestriction(LineRestriction.None);

            if (l2 != null && l1 != null)
                Lines.Insert(Lines.IndexOf(l1), new LineControl(this, l1.V1, l2.V2));

            Lines.Remove(l2);
            Lines.Remove(l1);
            Vertices.Remove(vertex);

            Draw();
        }

        public void MoveVertex(Vertex vertex)
        {
            ParentCanvas.RemoveHelperLines();
            ApplyLineRestrictions(vertex);
            CheckVerticesOnLines(vertex);
            Draw();
        }

        private void CheckVerticesOnLines(Vertex vertex)
        {
            ParentCanvas.RemoveHelperLines();
            foreach(var v in Vertices)
            {
                if(v != vertex && Math.Abs(v.Position.X - vertex.Position.X) < 5)
                {
                    ParentCanvas.DrawHelperVerticalLine(v.Position.X);
                }

                if (v != vertex && Math.Abs(v.Position.Y - vertex.Position.Y) < 5)
                {
                    ParentCanvas.DrawHelperHorizontalLine(v.Position.Y);
                }
            }
        }

        public bool IsProperlyRestricted()
        {
            var last = LineRestriction.None;
            foreach (var line in Lines)
            {
                if (line.Restriction == last && last != LineRestriction.None && last != LineRestriction.Angle)
                    return false;

                if (line.V1.IsRestricted && line.V2.IsRestricted)
                    return false;

                if (line.Restriction != LineRestriction.None && line.Restriction != LineRestriction.Angle && (line.V1.IsRestricted || line.V2.IsRestricted))
                    return false;
                last = line.Restriction;
            }
            if (Lines[0].Restriction == last && last != LineRestriction.None && last != LineRestriction.Angle)
                return false;
            return true;
        }

        internal void RestrictAngleLines(Vertex vertex, LineRestriction restriction)
        {
            var l1 = Lines.FirstOrDefault(l => l.V1 == vertex);
            var l2 = Lines.FirstOrDefault(l => l.V2 == vertex);
            l1.SetRestriction(restriction);
            l2.SetRestriction(restriction);
        }

        public void ApplyLineRestrictions(Vertex vertex)
        {
            var line = Lines.FirstOrDefault(l => l.V1 == vertex);
            if (line == null)
                return;

            var ind = Lines.IndexOf(line);
            bool[] visited = new bool[Lines.Count];

            int it = ind;
            while (visited[it] == false && Lines[it].Restriction != LineRestriction.None && Lines[it].V2 != vertex)
            {
                ApplyRestrictionAt(it);

                visited[it] = true;
                it = (it + 1) % Lines.Count;
            }

            visited = new bool[Lines.Count];

            it = ind == 0 ? Lines.Count - 1 : ind - 1;
            while (visited[it] == false && Lines[it].Restriction != LineRestriction.None && Lines[it].V1 != vertex)
            {
                ApplyRestrictionBackwardAt(it);

                visited[it] = true;
                it = it == 0 ? Lines.Count - 1 : it - 1;
            }
        }

        private void ApplyRestrictionAt(int it)
        {
            if (Lines[it].Restriction == LineRestriction.Horizontal)
            {
                Lines[it].V2.Position = new Point(Lines[it].V2.Position.X, Lines[it].V1.Position.Y);
            }
            if (Lines[it].Restriction == LineRestriction.Vertical)
            {
                Lines[it].V2.Position = new Point(Lines[it].V1.Position.X, Lines[it].V2.Position.Y);
            }

            if (Lines[it].Restriction == LineRestriction.Angle)
            {
                var a1 = Lines[it].GetSlope();
                var a2 = Lines[(it + 1) % Lines.Count].GetSlope();

                double X, Y;

                if (!double.IsNaN(a1) && !double.IsNaN(a2))
                {
                    X = (a1 * Lines[it].V1.Position.X - Lines[it].V1.Position.Y - a2 * Lines[it].V2.Position.X + Lines[it].V2.Position.Y) / (a1 - a2);
                    Y = a1 * (X - Lines[it].V1.Position.X) + Lines[it].V1.Position.Y;
                }
                if (double.IsNaN(a1) && !double.IsNaN(a2))
                {
                    X = Lines[it].V1.Position.X;
                    Y = a2 * (X - Lines[it].V2.Position.X) + Lines[it].V2.Position.Y;
                }
                else
                {
                    X = Lines[it].V2.Position.X;
                    Y = a1 * (X - Lines[it].V1.Position.X) + Lines[it].V1.Position.Y;
                }

                Lines[it].V2.Position = new Point(X, Y);
            }
        }

        private void ApplyRestrictionBackwardAt(int it)
        {
            if (Lines[it].Restriction == LineRestriction.Horizontal)
            {
                Lines[it].V1.Position = new Point(Lines[it].V1.Position.X, Lines[it].V2.Position.Y);
            }
            if (Lines[it].Restriction == LineRestriction.Vertical)
            {
                Lines[it].V1.Position = new Point(Lines[it].V2.Position.X, Lines[it].V1.Position.Y);
            }

            if (Lines[it].Restriction == LineRestriction.Angle)
            {
                var a1 = Lines[it].GetSlope();
                var a2 = Lines[(it + 1) % Lines.Count].GetSlope();

                double X, Y;

                if (!double.IsNaN(a1) && !double.IsNaN(a2))
                {
                    X = (a1 * Lines[it].V2.Position.X - Lines[it].V2.Position.Y - a2 * Lines[it].V1.Position.X + Lines[it].V1.Position.Y) / (a1 - a2);
                    Y = a1 * (X - Lines[it].V2.Position.X) + Lines[it].V2.Position.Y;
                }
                if (double.IsNaN(a1) && !double.IsNaN(a2))
                {
                    X = Lines[it].V2.Position.X;
                    Y = a2 * (X - Lines[it].V1.Position.X) + Lines[it].V1.Position.Y;
                }
                else
                {
                    X = Lines[it].V1.Position.X;
                    Y = a1 * (X - Lines[it].V2.Position.X) + Lines[it].V2.Position.Y;
                }
                Lines[it].V1.Position = new Point(X, Y);

            }
        }

        public void Draw()
        {
            ParentCanvas.DrawPolygon();
        }

        internal void TryClosePolygon(Vertex vertex)
        {
            if (vertex != Vertices.First() || Vertices.Count < 3)
                return;
            var v1 = Vertices.Last();
            var v2 = vertex;

            IsClosed = true;
            Lines.Add(new LineControl(this, v1, v2));
            Draw();
        }

        internal void SplitLine(LineControl line)
        {
            var position = new Point((line.V1.Position.X + line.V2.Position.X) / 2, (line.V1.Position.Y + line.V2.Position.Y) / 2);

            var eq = line.GetGeneralFormEquation();
            Vector vector = new Vector(eq.A, eq.B);
            vector.Normalize();
            position.X += 20 * vector.X;
            position.Y += 20 * vector.Y;

            var index1 = Vertices.FindIndex(v => line.V1 == v);
            var index2 = Vertices.FindIndex(v => line.V2 == v);
            var lineIndex = Lines.FindIndex(l => l == line);
            var ver = new Vertex(position, this);
            var l1 = new LineControl(this, line.V1, ver);
            var l2 = new LineControl(this, ver, line.V2);

            if (line.Restriction == LineRestriction.Angle)
            {
                if (line.V1.IsRestricted)
                    line.V1.SetRestriction(LineRestriction.None);
                else
                    line.V2.SetRestriction(LineRestriction.None);

            }

            var newIndex = Math.Abs(index1 - index2) > 1 ? 0 : Math.Max(index1, index2);
            Vertices.Insert(newIndex, ver);

            Lines.Remove(line);
            Lines.Insert(lineIndex, l1);
            Lines.Insert(lineIndex + 1, l2);

            foreach (var l in Lines)
                Draw();
        }
    }
}
