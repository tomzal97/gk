﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GK1.Models;

namespace GK1.Controls
{
    /// <summary>
    /// Interaction logic for Vertex.xaml
    /// </summary>
    public partial class Vertex : UserControl, INotifyPropertyChanged
    {
        static public int Size { get; set; } = 15;

        private readonly Brush _inactiveColor = Brushes.Black;

        private readonly Brush _activeColor = Brushes.Red;

        private readonly Brush _restrictedColor = Brushes.Blue;

        public Visibility IconVisibility { get; set; } = Visibility.Hidden;

        public Brush Color
        {
            get => _color;
            set => SetProperty(ref _color, value);
        }

        private Brush _color;

        public Point Position { get; set; }

        public bool IsRestricted { get; set; }
        public Polygon ParentPolygon { get; set; }

        private bool _isChoosed = false;

        public event PropertyChangedEventHandler PropertyChanged;

        public Vertex(Point pos, Models.Polygon parent)
        {
            InitializeComponent();
            DataContext = this;
            Position = pos;
            ParentPolygon = parent;
            _color = _inactiveColor;
            Panel.SetZIndex(this, 10);
        }

        private void ActivateVertex(object sender, MouseButtonEventArgs e)
        {
            if (!ParentPolygon.IsClosed)
            {
                ParentPolygon.TryClosePolygon(this);
                return;
            }

            _isChoosed = !_isChoosed;
            Color = _isChoosed ? _activeColor : _inactiveColor;
        }

        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            Position = new Point(Canvas.GetLeft(this) + e.HorizontalChange, Canvas.GetTop(this) + e.VerticalChange);

            Color = _activeColor;
            ParentPolygon.MoveVertex(this);
        }

        private void SetRestriction(object sender, RoutedEventArgs e)
        {
            var choosenOption = ((MenuItem)sender).Tag.ToString();

            if (choosenOption == "None")
                SetRestriction(LineRestriction.None);
            else
                SetRestriction(LineRestriction.Angle);
        }

        public void SetRestriction(LineRestriction restriction)
        {
            if (!ParentPolygon.IsClosed)
            {
                MessageBox.Show("Polygon must be closed", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (restriction == LineRestriction.None)
                IsRestricted = false;
            else
                IsRestricted = true;

            if (!ParentPolygon.IsProperlyRestricted())
            {
                IsRestricted = false;
                MessageBox.Show("Can not restrict angle", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
            }

            ParentPolygon.RestrictAngleLines(this, IsRestricted ? LineRestriction.Angle : LineRestriction.None);
            IconVisibility = IsRestricted ? Visibility.Visible : Visibility.Hidden;

            OnPropertyChanged(nameof(IconVisibility));
            Color = IsRestricted ? _restrictedColor : _inactiveColor;
        }

        private void Thumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            Color = IsRestricted ? _restrictedColor : _inactiveColor;

            if(ParentPolygon.ParentCanvas.HelperVerticalLine.Visibility == Visibility.Visible)
            {
                Position = new Point(ParentPolygon.ParentCanvas.HelperVerticalLine.X1, Position.Y);
                ParentPolygon.MoveVertex(this);
            }

            if (ParentPolygon.ParentCanvas.HelperHorizontalLine.Visibility == Visibility.Visible)
            {
                Position = new Point(Position.X, ParentPolygon.ParentCanvas.HelperHorizontalLine.Y1);
                ParentPolygon.MoveVertex(this);
            }

            ParentPolygon.ParentCanvas.RemoveHelperLines();
        }
        private void RemoveVertexOnWheelClick(object sender, MouseButtonEventArgs e)
        {
            if (e.MiddleButton == MouseButtonState.Pressed)
                ParentPolygon.RemoveVertex(this);
        }

        #region NotifyPropertyChangedImplementation

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

    }
}
