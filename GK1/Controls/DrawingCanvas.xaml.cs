﻿using GK1.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GK1.Controls
{
    /// <summary>
    /// Interaction logic for DrawingCanvas.xaml
    /// </summary>
    public partial class DrawingCanvas : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _bitmap;

        public WriteableBitmap CanvasBitmap
        {
            get => _bitmap;
            set => SetProperty(ref _bitmap, value);
        }

        public BitmapDrawer Drawer { get; private set; }
        public Models.Polygon Polygon { get; set; }

        public DrawingCanvas()
        {
            InitializeComponent();
            DataContext = this;
            Polygon = new Models.Polygon(this);
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                CanvasBitmap = new WriteableBitmap((int)canvas.ActualWidth, (int)canvas.ActualHeight, 12, 12, PixelFormats.Rgb24, null);
                Drawer = new BitmapDrawer(CanvasBitmap);
                Drawer.Clear();
                CreateExamplePolygon();
            }));
        }

        public void DrawPolygon()
        {
            canvas.Children.Clear();
            canvas.Children.Add(HelperVerticalLine);
            canvas.Children.Add(HelperHorizontalLine);
            Drawer.Clear();

            var vertices = Polygon.GetVertices();
            var lines = Polygon.GetLines();

            foreach (var v in vertices)
                DrawVertex(v);

            foreach (var l in lines)
                DrawLine(l);

            Drawer.WritePixelsToBitmap();
        }

        public void DrawVertex(Vertex vertex)
        {
            canvas.Children.Add(vertex);
            Canvas.SetLeft(vertex, vertex.Position.X - Vertex.Size / 2);
            Canvas.SetTop(vertex, vertex.Position.Y - Vertex.Size / 2);
        }

        public void DrawLine(LineControl line)
        {
            line.Refresh();
            var x1 = (int)line.V1.Position.X;
            var y1 = (int)line.V1.Position.Y;
            var x2 = (int)line.V2.Position.X;
            var y2 = (int)line.V2.Position.Y;

            var lineColor = line.LineColor as SolidColorBrush;
            var R = lineColor.Color.R;
            var G = lineColor.Color.G;
            var B = lineColor.Color.B;

            DrawLine(x1, y1, x2, y2, (R, G, B));
            canvas.Children.Add(line);
        }

        public Line HelperVerticalLine { get; set; } = new Line();

        public Line HelperHorizontalLine { get; set; } = new Line();

        internal void DrawHelperVerticalLine(double X)
        {
            HelperVerticalLine.Stroke = Brushes.Blue;
            HelperVerticalLine.StrokeDashArray = new DoubleCollection() { 2 };
            HelperVerticalLine.Visibility = Visibility.Visible;
            HelperVerticalLine.X1 = X;
            HelperVerticalLine.X2 = X;
            HelperVerticalLine.Y1 = 0;
            HelperVerticalLine.Y2 = canvas.ActualHeight;
        }

        internal void DrawHelperHorizontalLine(double Y)
        {
            HelperHorizontalLine.Stroke = Brushes.Red;
            HelperHorizontalLine.StrokeDashArray = new DoubleCollection() { 2 };
            HelperHorizontalLine.Visibility = Visibility.Visible;
            HelperHorizontalLine.X1 = 0;
            HelperHorizontalLine.X2 = canvas.ActualWidth;
            HelperHorizontalLine.Y1 = Y;
            HelperHorizontalLine.Y2 = Y;
        }

        internal void RemoveHelperLines()
        {
            HelperVerticalLine.Visibility = Visibility.Hidden;
            HelperHorizontalLine.Visibility = Visibility.Hidden;
        }

        public void DrawLine(int x1, int y1, int x2, int y2, (int R, int G, int B) color)
        {
            int dx = Math.Abs(x2 - x1);
            int sx = x1 < x2 ? 1 : -1;
            int dy = Math.Abs(y2 - y1);
            int sy = y1 < y2 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2;
            int e2 = err;

            while (x1 != x2 || y1 != y2)
            {
                Drawer.SetPixel(x1, y1, color);
                e2 = err;
                if (e2 > -dx)
                {
                    err -= dy;
                    x1 += sx;
                }
                if (e2 < dy)
                {
                    err += dx;
                    y1 += sy;
                }
            }
            Drawer.SetPixel(x1, y1, color);

        }

        public void CreateExamplePolygon()
        {
            Models.Polygon poly = new Models.Polygon(this);
            List<Vertex> vertices = new List<Vertex>();
            vertices.Add(new Vertex(new Point(850, 100), poly));
            vertices.Add(new Vertex(new Point(850, 500), poly));
            vertices.Add(new Vertex(new Point(550, 500), poly));
            vertices.Add(new Vertex(new Point(600, 620), poly));
            vertices.Add(new Vertex(new Point(290, 400), poly));
            vertices.Add(new Vertex(new Point(400, 250), poly));

            foreach (var v in vertices)
                poly.AddVertex(v);

            poly.TryClosePolygon(vertices.First());

            var lines = poly.GetLines();

            lines[0].SetRestriction(LineRestriction.Vertical);
            lines[1].SetRestriction(LineRestriction.Horizontal);
            vertices[4].SetRestriction(LineRestriction.Angle);

            Polygon = poly;
            DrawPolygon();
        }

        public void ClearCanvas()
        {
            Polygon = new Models.Polygon(this);
            DrawPolygon();
            Drawer.Clear();
        }

        public void ExportPolygon()
        {
            if(!Polygon.IsClosed)
            {
                MessageBox.Show("Polygon must be closed", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }
            List<VertexInfo> verticesInfo = new List<VertexInfo>();
            List<LineInfo> linesInfo = new List<LineInfo>();

            foreach(var v in Polygon.Vertices)
            {
                verticesInfo.Add(new VertexInfo()
                {
                    Position = v.Position,
                    IsRestricted = v.IsRestricted
                });
            }
            foreach(var l in Polygon.Lines)
            {
                linesInfo.Add(new LineInfo()
                {
                    Restriction = l.Restriction
                });
            }

            JsonSerializer serializer = new JsonSerializer();

            serializer.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;

            using (StreamWriter sw = new StreamWriter(@"vertices.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, verticesInfo);
                // {"ExpiryDate":new Date(1230375600000),"Price":0}
            }

            using (StreamWriter sw = new StreamWriter(@"lines.json"))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, linesInfo);
                // {"ExpiryDate":new Date(1230375600000),"Price":0}
            }
        }

        public void ImportPolygon()
        {
            Polygon = new Models.Polygon(this);
            List<VertexInfo> verticesInfo = JsonConvert.DeserializeObject<List<VertexInfo>>(File.ReadAllText("vertices.json"));
            List<Vertex> vertices = new List<Vertex>();

            foreach (var v in verticesInfo)
            {
                var vertex = new Vertex(v.Position, Polygon);
                vertices.Add(vertex);
                Polygon.AddVertex(vertex);
            }

            Polygon.TryClosePolygon(Polygon.Vertices[0]);
            for(int i=0; i<verticesInfo.Count; i++)
            {
                if (verticesInfo[i].IsRestricted)
                    vertices[i].SetRestriction(LineRestriction.Angle);
            }

            var lines = JsonConvert.DeserializeObject<List<LineInfo>>(File.ReadAllText("lines.json"));
            for(int i=0; i<lines.Count; i++)
            {
                var p = lines[i];
                Polygon.Lines[i].SetRestriction(p.Restriction);
            }
            DrawPolygon();
        }

        private void PlaceVertexOnCanvas(object sender, MouseButtonEventArgs e)
        {
            if (e.OriginalSource is Ellipse || Polygon.IsClosed)
                return;

            var clickedPosition = e.GetPosition(canvas);
            Vertex vertex = new Vertex(clickedPosition, Polygon);

            Panel.SetZIndex(vertex, 10);
            Polygon.AddVertex(vertex);
        }

        #region NotifyPropertyChangedImplementation

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

    }
}
