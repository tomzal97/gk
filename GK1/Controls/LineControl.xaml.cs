﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GK1.Models;

namespace GK1.Controls
{
    /// <summary>
    /// Interaction logic for LineControl.xaml
    /// </summary>
    public partial class LineControl : UserControl, INotifyPropertyChanged
    {
        private List<Brush> _colors = new List<Brush> { Brushes.Black, Brushes.Red, Brushes.Blue, Brushes.Green };

        public Point IconPosition => new Point((V1.Position.X + V2.Position.X) / 2, (V1.Position.Y + V2.Position.Y) / 2);

        private BitmapImage _restrictionIcon;

        public BitmapImage RestrictionIcon
        {
            get => _restrictionIcon;
            set => SetProperty(ref _restrictionIcon, value);
        }

        private Brush _color = Brushes.Black;

        public Brush LineColor
        {
            get => _color;
            set => SetProperty(ref _color, value);
        }

        public Vertex V1 { get; set; }

        public Vertex V2 { get; set; }

        public LineRestriction Restriction { get; set; } = LineRestriction.None;

        public Models.Polygon ParentPolygon { get; set; }

        private double _fixedSlope;

        public LineControl(Models.Polygon parent)
        {
            InitializeComponent();
            DataContext = this;
            Panel.SetZIndex(this, 1);

            ParentPolygon = parent;

            OnPropertyChanged(nameof(LineColor));
        }

        public LineControl(Models.Polygon parent, Vertex v1, Vertex v2) : this(parent)
        {
            V1 = v1;
            V2 = v2;
        }

        public (double A, double B, double C) GetGeneralFormEquation()
        {
            var A = V1.Position.Y - V2.Position.Y;
            var B = V2.Position.X - V1.Position.X;
            var C = V1.Position.X * V2.Position.Y - V2.Position.X * V1.Position.Y;
            return (A, B, C);
        }

        public double GetSlope()
        {
            if (Restriction == LineRestriction.Angle)
                return _fixedSlope;
            else
            {
                if (V1.Position.X == V2.Position.X)
                    return double.NaN;
                else
                    return (V2.Position.Y - V1.Position.Y) / (V2.Position.X - V1.Position.X);
            }
        }

        public void Refresh()
        {
            OnPropertyChanged(nameof(V1));
            OnPropertyChanged(nameof(V2));
            OnPropertyChanged(nameof(IconPosition));
            OnPropertyChanged(nameof(RestrictionIcon));
        }

        private void AddVertexOnLineDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount != 2)
                return;

            ParentPolygon.SplitLine(this);
        }

        private void SetRestriction(object sender, RoutedEventArgs e)
        {
            var choosenOption = ((MenuItem)sender).Tag.ToString();
            LineRestriction restriction;
            Enum.TryParse(choosenOption, out restriction);
            SetRestriction(restriction);
        }

        public void SetRestriction(LineRestriction restriction)
        {
            if(!ParentPolygon.IsClosed)
            {
                MessageBox.Show("Polygon must be closed", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                return;
            }

            if (restriction == LineRestriction.Angle)
            {
                _fixedSlope = GetSlope();
            }

            Restriction = restriction;
            
            if (!ParentPolygon.IsProperlyRestricted())
            {
                MessageBox.Show("Can not set the restriction!", "Alert", MessageBoxButton.OK, MessageBoxImage.Information);
                Restriction = LineRestriction.None;
            }
            else
            {
                LineColor = _colors[(int)Restriction];
                SetImageFromRestriction();
                ParentPolygon.ApplyLineRestrictions(V1);
                ParentPolygon.ApplyLineRestrictions(V2);
                ParentPolygon.Draw();
            }
        }

        private void SetImageFromRestriction()
        {
            switch (Restriction)
            {
                case LineRestriction.None:
                    RestrictionIcon = null;
                    return;
                case LineRestriction.Horizontal:
                    RestrictionIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/Horizontal.png"));
                    return;
                case LineRestriction.Vertical:
                    RestrictionIcon = new BitmapImage(new Uri("pack://application:,,,/Resources/Vertical.png"));
                    return;
            }
        }

        #region NotifyPropertyChangedImplementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion
    }

    public enum LineRestriction
    {
        None = 0,
        Horizontal,
        Vertical,
        Angle
    }
}
