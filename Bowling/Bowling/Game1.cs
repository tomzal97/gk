﻿using Bowling.Cameras;
using Bowling.Materials;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Projekt4;
using System;
using System.Collections.Generic;

namespace Bowling
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        List<CModel> sceneModels = new List<CModel>();
        List<CModel> pins = new List<CModel>();
        CModel bowlingPin, bowlingBall, basicFloor;
        Camera camera, chaseCamera, targetCamera, fixedCamera, freeCamera;
        Floor floor;
        Terrain terrain;
        Effect floorEffect;
        Sphere sphere;
        MouseState lastMouseState;

        bool isFixedCamera = false;

        Vector3 fixedCameraPosition = new Vector3(20, 5, 16);
        Vector3 fixedCameraTarget = new Vector3(10, 1, 12);
        Vector3 initialBallPosition = new Vector3(11.25f, 0.25f, 17);

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            lastMouseState = Mouse.GetState();

            var floorTexture = Content.Load<Texture2D>("Textures/WoodTexture");
            floorEffect = Content.Load<Effect>("Effects/TextureMapping");
            floorEffect = new BasicEffect(GraphicsDevice);

            floorEffect = Content.Load<Effect>("Effects/PointLightEffect");
            floor = new Floor(GraphicsDevice, floorTexture);
            floor.Material = new PointLightMaterial()
            {
                LightPosition = initialBallPosition,
                LightAttenuation = 5,
                LightColor = Color.Blue.ToVector3()
            };

            AddFloor();
            AddPins();
            AddSceneModels();
            AddBall();
            PrepareCameras();
            camera = chaseCamera;
        }

        private void AddFloor()
        {
            Effect someEffect = Content.Load<Effect>("Effects/SpotLightEffect");
            //var material = new MultiLightingMaterial()
            //{
            //    LightPosition = new Vector3(11.25f, 6, 5.15f),
            //    LightDirection = new Vector3(0,-1f, 0.5f),
            //    LightColor = Color.Red.ToVector3(),
            //    LightFalloff = 4
            //};
            //material.Lig

            var floorTexture = Content.Load<Texture2D>("Textures/WoodTexture");
            basicFloor = new CModel(Content.Load<Model>("Models/BasicFloor"), new Vector3(0, -4.05f, 7), Vector3.Zero, new Vector3(0.04f, 0.04f, 0.05f), GraphicsDevice) { Texture = floorTexture };
           
            basicFloor.Material = material;
            basicFloor.SetModelEffect(someEffect, true);
        }

        private void AddBall()
        {
            //Effect someEffect = Content.Load<Effect>("Effects/TextureMapping");
            //LightingMaterial material = new LightingMaterial()
            //{

            //};
            Effect someEffect = Content.Load<Effect>("Effects/SpotLightEffect");
            var material = new SpotLightMaterial()
            {
                LightPosition = new Vector3(11.25f, 6, 5.15f),
                LightDirection = new Vector3(0, -MathHelper.ToRadians(45), 0f),
                LightColor = Color.Red.ToVector3(),
                LightFalloff = 10
            };

            bowlingBall = new CModel(Content.Load<Model>("Models/BowlingBall"), initialBallPosition, new Vector3(MathHelper.ToRadians(0), -MathHelper.ToRadians(0), 0), new Vector3(0.2f), GraphicsDevice);
            initialBallPosition.Y = bowlingBall.BoundingSphere.Radius;
            bowlingBall.Position = initialBallPosition;
            bowlingBall.Material = material;
            bowlingBall.SetModelEffect(someEffect, true);
        }

        private void PrepareCameras()
        {
            chaseCamera = new ChaseCamera(new Vector3(0, 0.8f, 2.5f), new Vector3(0, 0f, 0), Vector3.Zero, GraphicsDevice);
            targetCamera = new TargetCamera(fixedCameraPosition, bowlingBall.Position, GraphicsDevice);
            fixedCamera = new TargetCamera(fixedCameraPosition, fixedCameraTarget, GraphicsDevice);
            freeCamera = new FreeCamera(new Vector3(10, 5, 10), 0, 0, GraphicsDevice);
        }

        private void AddSceneModels()
        {
            sceneModels = new List<CModel>()
            {
                new CModel(Content.Load<Model>("Models/BowlingTrophy"), new Vector3(4,-0.1f,4), new Vector3(-MathHelper.ToRadians(90), -MathHelper.ToRadians(45),0), new Vector3(0.1f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/Table"), new Vector3(4,0.9f,14), new Vector3(0, 0,0), new Vector3(0.02f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/BowlingBowl"), new Vector3(13,0,17), new Vector3(0, MathHelper.ToRadians(15),0), new Vector3(1.05f), GraphicsDevice),
            };

            sceneModels.Add(new CModel(Content.Load<Model>("Models/Table"), new Vector3(4, 0.9f, 20), new Vector3(0, 0, 0), new Vector3(0.02f), GraphicsDevice));

            Effect someEffect = Content.Load<Effect>("Effects/TextureMapping");
            LightingMaterial material = new LightingMaterial()
            {
            };
            
            foreach(var model in sceneModels)
            {
                model.Material = material;
                model.SetModelEffect(someEffect, true);
            }
        }

        private void AddPins()
        {
            pins = new List<CModel>()
            {
                new CModel(Content.Load<Model>("Models/BowlingPin"), new Vector3(11, 0f, 5), new Vector3(0, 0, 0), new Vector3(0.05f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/BowlingPin"), new Vector3(11.5f, 0f, 5), new Vector3(0, 0, 0), new Vector3(0.05f), GraphicsDevice),
                new CModel(Content.Load<Model>("Models/BowlingPin"), new Vector3(11.25f, 0f, 5.3f), new Vector3(0, 0, 0), new Vector3(0.05f), GraphicsDevice)
            };

            LightingMaterial material = new LightingMaterial()
            {
                
            };

            Effect someEffect = Content.Load<Effect>("Effects/TextureMapping");
            pins[0].Material = material;
            pins[0].SetModelEffect(someEffect, true);
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            UpdateSettings(gameTime);
            UpdateModel(gameTime);
            UpdateCamera(gameTime);
            base.Update(gameTime);
        }

        private void UpdateCamera(GameTime gameTime)
        {
            switch (camera)
            {
                case FreeCamera c:
                    UpdateFreeCamera(gameTime);
                    break;
                case ChaseCamera c:
                    UpdateChaseCamera(gameTime);
                    break;
                case TargetCamera c:
                    UpdateTargetCamera(gameTime);
                    break;
                default:
                    break;

            }
        }

        private void UpdateTargetCamera(GameTime gameTime)
        {
            if (isFixedCamera)
            {
                ((TargetCamera)camera).Target = fixedCameraTarget;
            }
            else
            {
                ((TargetCamera)camera).Target = bowlingBall.Position;
            }
            camera.Update();
        }

        private void UpdateSettings(GameTime gameTime)
        {
            var keyState = Keyboard.GetState();

            if (keyState.IsKeyDown(Keys.F1))
            {
                camera = chaseCamera;
            }
            if (keyState.IsKeyDown(Keys.F2))
            {
                camera = targetCamera;
                isFixedCamera = false;
            }
            if (keyState.IsKeyDown(Keys.F3))
            {
                camera = fixedCamera;
                isFixedCamera = true;
            }
            if (keyState.IsKeyDown(Keys.F4))
            {
                camera = freeCamera;
            }
            if (keyState.IsKeyDown(Keys.R))
            {
                bowlingBall.Position = initialBallPosition;
            }

        }

        private void UpdateModel(GameTime gameTime)
        {
            KeyboardState keyState = Keyboard.GetState();

            if (!keyState.IsKeyDown(Keys.Space))
                return;
            var rot = new Vector3(-1, 0, 0);

            bowlingBall.Rotation += rot * 0.2f;
            bowlingBall.Position += Vector3.Forward * 5f * (float)gameTime.ElapsedGameTime.TotalSeconds; ;
        }

        private void UpdateChaseCamera(GameTime gameTime)
        {
            ((ChaseCamera)camera).Move(bowlingBall.Position, Vector3.Zero);
            camera.Update();
        }

        private void UpdateFreeCamera(GameTime gameTime)
        {
            MouseState mouseState = Mouse.GetState();
            KeyboardState keyState = Keyboard.GetState();

            float deltaX = (float)lastMouseState.X - (float)mouseState.X;
            float deltaY = (float)lastMouseState.Y - (float)mouseState.Y;
            
            ((FreeCamera)camera).Rotate(deltaX * .005f, deltaY * .005f);
            Vector3 translation = Vector3.Zero;
            
            if (keyState.IsKeyDown(Keys.W)) translation += Vector3.Forward;
            if (keyState.IsKeyDown(Keys.S)) translation += Vector3.Backward;
            if (keyState.IsKeyDown(Keys.A)) translation += Vector3.Left;
            if (keyState.IsKeyDown(Keys.D)) translation += Vector3.Right;
            
            translation *= 20f * (float)gameTime.ElapsedGameTime.TotalSeconds;
            ((FreeCamera)camera).Move(translation);
            camera.Update();
            lastMouseState = mouseState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            RasterizerState rasterizerState = new RasterizerState();
            rasterizerState.CullMode = CullMode.None;
            GraphicsDevice.RasterizerState = rasterizerState;

            DrawModels();

            bowlingBall.Draw(camera.View, camera.Projection, camera.Position);
            basicFloor.Draw(camera.View, camera.Projection, camera.Position);
            // floor.Draw(camera, floorEffect);
            //terrain.Draw(camera);
            //sphere.Draw(camera);
            base.Draw(gameTime);
        }

        private void DrawModels()
        {
            foreach (var pin in pins)
            {
                pin.Draw(camera.View, camera.Projection, camera.Position);
            }

            foreach (var model in sceneModels)
            {
                model.Draw(camera.View, camera.Projection, camera.Position);
            }
        }
    }
}
