﻿using OxyPlot;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;

namespace Lab1.Models
{
    public class ImageEditor : BitmapDrawer
    {

        double[] _fours = new double[]
        {
            3, 6, 12, 24, 48, 96, 192, 129,
            7, 14, 28, 56, 112, 224, 193, 131,
            15, 30, 60, 120, 240, 225, 195, 135
        };

        double[] _deleteValues = new double[]
        {
            3, 5, 7, 12, 13, 14, 15, 20,
            21, 22, 23, 28, 29, 30, 31, 48,
            52, 53, 54, 55, 56, 60, 61, 62,
            63, 65, 67, 69, 71, 77, 79, 80,
            81, 83, 84, 85, 86, 87, 88, 89,
            91, 92, 93, 94, 95, 97, 99, 101,
            103, 109, 111, 112, 113, 115, 116, 117,
            118, 119, 120, 121, 123, 124, 125, 126,
            127, 131, 133, 135, 141, 143, 149, 151,
            157, 159, 181, 183, 189, 191, 192, 193,
            195, 197, 199, 205, 207, 208, 209, 211,
            212, 213, 214, 215, 216, 217, 219, 220,
            221, 222, 223, 224, 225, 227, 229, 231,
            237, 239, 240, 241, 243, 244, 245, 246,
            247, 248, 249, 251, 252, 253, 254, 255
        };

        public ImageEditor(WriteableBitmap image) : base(image)
        {

        }

        public void Grayscale()
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    int avg = Truncate((color.R + color.G + color.B) / 3);
                    SetPixel(x, y, new PixelColorInfo(avg, avg, avg, 255));
                }
            }
            WritePixelsToBitmap();
        }

        public void Negative()
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    color.R = 255 - color.R;
                    color.G = 255 - color.G;
                    color.B = 255 - color.B;
                    SetPixel(x, y, color);
                }
            }
            WritePixelsToBitmap();
        }

        public List<List<DataPoint>> GetHistograms()
        {
            List<List<DataPoint>> histograms = new List<List<DataPoint>>()
            {
                new List<DataPoint>(),
                new List<DataPoint>(),
                new List<DataPoint>(),
                new List<DataPoint>()
            };

            int[] counterR = new int[256];
            int[] counterG = new int[256];
            int[] counterB = new int[256];
            int[] counterBW = new int[256];

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    counterR[color.R]++;
                    counterG[color.G]++;
                    counterB[color.B]++;
                    var BW = (color.R + color.G + color.B) / 3;
                    counterBW[BW]++;
                }
            }
            for (int i = 0; i <= 255; i++)
            {
                histograms[0].Add(new DataPoint(i, counterR[i]));
                histograms[1].Add(new DataPoint(i, counterG[i]));
                histograms[2].Add(new DataPoint(i, counterB[i]));
                histograms[3].Add(new DataPoint(i, counterBW[i]));
            }
            return histograms;
        }

        public void ChangeBrigthness(double brightness)
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    color.R = Truncate(color.R + brightness);
                    color.G = Truncate(color.G + brightness);
                    color.B = Truncate(color.B + brightness);
                    SetPixel(x, y, color);
                }
            }
        }

        public void Treshold(double value)
        {
            Grayscale();
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    if (color.R < value)
                        SetPixel(x, y, new PixelColorInfo(0, 0, 0, 255));
                    else
                        SetPixel(x, y, new PixelColorInfo(255, 255, 255, 255));
                }
            }
        }

        public void ChangeContrast(double contrast)
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    //double factor = (259 * ((double)contrast + 255)) / (255 * (259 - (double)contrast));
                    double factor = contrast;
                    var color = GetPixel(x, y);
                    color.R = Truncate((color.R - 128) * factor + 128);
                    color.G = Truncate((color.G - 128) * factor + 128);
                    color.B = Truncate((color.B - 128) * factor + 128);
                    SetPixel(x, y, color);
                }
            }
        }

        public void ChangeContrastExp(double contrast)
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    color.R = Truncate(255 * Math.Pow((double)color.R / 255, contrast));
                    color.G = Truncate(255 * Math.Pow((double)color.G / 255, contrast));
                    color.B = Truncate(255 * Math.Pow((double)color.B / 255, contrast));
                    SetPixel(x, y, color);
                }
            }
        }

        public void Normalization()
        {
            var l = GetColorLimits();

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    color.R = Truncate((255 / (l.maxR - l.minR)) * (color.R - l.minR));
                    color.G = Truncate((255 / (l.maxG - l.minG)) * (color.G - l.minG));
                    color.B = Truncate((255 / (l.maxB - l.minB)) * (color.B - l.minB));
                    SetPixel(x, y, color);
                }
            }
            WritePixelsToBitmap();
        }

        public void Equalization()
        {
            var histograms = GetHistograms();
            long[] cumulativeR = histograms[0].Select(p => (long)p.Y).ToArray();
            long[] cumulativeG = histograms[1].Select(p => (long)p.Y).ToArray();
            long[] cumulativeB = histograms[2].Select(p => (long)p.Y).ToArray();

            for (int i = 1; i < cumulativeB.Length; i++)
            {
                cumulativeR[i] += cumulativeR[i - 1];
                cumulativeG[i] += cumulativeG[i - 1];
                cumulativeB[i] += cumulativeB[i - 1];
            }

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    color.R = (int)Math.Round((double)(cumulativeR[color.R] - cumulativeR[0]) * 255 / (cumulativeR[255] - cumulativeR[0]));
                    color.G = (int)Math.Round((double)(cumulativeG[color.G] - cumulativeG[0]) * 255 / (cumulativeG[255] - cumulativeG[0]));
                    color.B = (int)Math.Round((double)(cumulativeG[color.B] - cumulativeB[0]) * 255 / (cumulativeB[255] - cumulativeB[0]));
                    SetPixel(x, y, color);
                }
            }
            WritePixelsToBitmap();
        }

        public void AveragingFilter()
        {
            int[,] Mask = { { 1, 1, 1},
                            { 1, 1, 1 },
                            { 1, 1, 1 } };

            SetFilter(Mask);
        }

        public void GaussianFilter(int v)
        {
            int[,] Mask = { { 1, v, 1},
                            { v, v*v, v },
                            { 1, v, 1 } };

            SetFilter(Mask);
        }

        public void SharpeningFilter()
        {
            int[,] Mask = { { -1, -1, -1},
                            {-1, 9, -1},
                            {-1, -1, -1 } };

            SetFilter(Mask);
        }

        public void EdgeDetectionFilter()
        {
            PixelColorInfo[,] newPixelValues = new PixelColorInfo[bitmap.PixelWidth, bitmap.PixelHeight];

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    //int maskSum = 0;
                    PixelColorInfo val = new PixelColorInfo(0, 0, 0, 255);
                    var color = GetPixel(x, y);
                    var colorRight = GetPixel(x + 1, y);
                    var colorDown = GetPixel(x, y + 1);

                    val.R = Math.Abs((colorRight.R > 0 ? colorRight.R : 0) - color.R);
                    val.R = Math.Abs((colorDown.R > 0 ? colorRight.R : 0) - color.R);
                    val.G = Math.Abs((colorRight.G > 0 ? colorRight.G : 0) - color.G);
                    val.G = Math.Abs((colorDown.G > 0 ? colorRight.G : 0) - color.G);
                    val.B = Math.Abs((colorRight.B > 0 ? colorRight.B : 0) - color.B);
                    val.B = Math.Abs((colorDown.B > 0 ? colorRight.B : 0) - color.B);


                    newPixelValues[x, y] = val;
                }
            }

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    SetPixel(x, y, newPixelValues[x, y]);
                }
            }

            WritePixelsToBitmap();
        }

        public List<List<DataPoint>> GetProjections()
        {
            List<List<DataPoint>> projections = new List<List<DataPoint>>()
            {
                new List<DataPoint>(),
                new List<DataPoint>(),
            };

            int[] counterH = new int[bitmap.PixelWidth];
            int[] counterV = new int[bitmap.PixelHeight];

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    if (color.R == 0 && color.G == 0 && color.B == 0)
                    {
                        counterH[x]++;
                        counterV[y]++;
                    }
                }
            }

            for (int x = 0; x < bitmap.PixelWidth; x++)
                projections[0].Add(new DataPoint(x, counterH[x]));

            for (int y = 0; y < bitmap.PixelHeight; y++)
                projections[1].Add(new DataPoint(y, counterV[y]));

            return projections;
        }

        private void SetFilter(int[,] Mask)
        {
            (int x, int y) center = (Mask.GetLength(0) / 2, Mask.GetLength(1) / 2);

            PixelColorInfo[,] newPixelValues = new PixelColorInfo[bitmap.PixelWidth, bitmap.PixelHeight];

            var maskSum = 0;
            for (int i = 0; i < Mask.GetLength(0); i++)
            {
                for (int j = 0; j < Mask.GetLength(1); j++)
                    maskSum += Mask[i, j];
            }


            for (int x = 1; x < bitmap.PixelWidth - 1; x++)
            {
                for (int y = 1; y < bitmap.PixelHeight - 1; y++)
                {
                    //int maskSum = 0;
                    PixelColorInfo val = new PixelColorInfo(0, 0, 0, 255);

                    for (int mx = 0; mx < Mask.GetLength(0); mx++)
                    {
                        for (int my = 0; my < Mask.GetLength(1); my++)
                        {
                            var color = GetPixel(x + (mx - center.x), y + (my - center.y));
                            if (color.R == -1)
                                continue;

                            //maskSum += Mask[mx, my];
                            val.R += color.R * Mask[mx, my];
                            val.G += color.G * Mask[mx, my];
                            val.B += color.B * Mask[mx, my];
                        }
                    }

                    val.R /= (maskSum == 0 ? 1 : maskSum);
                    val.G /= (maskSum == 0 ? 1 : maskSum);
                    val.B /= (maskSum == 0 ? 1 : maskSum);
                    val.R = Truncate(Math.Abs(val.R));
                    val.G = Truncate(Math.Abs(val.G));
                    val.B = Truncate(Math.Abs(val.B));

                    newPixelValues[x, y] = val;
                }
            }

            for (int x = 1; x < bitmap.PixelWidth - 1; x++)
            {
                for (int y = 1; y < bitmap.PixelHeight - 1; y++)
                {
                    SetPixel(x, y, newPixelValues[x, y]);
                }
            }

            WritePixelsToBitmap();
        }

        public void KMM()
        {
            var colors = new int[bitmap.PixelWidth, bitmap.PixelHeight];
            SetColors(colors);

            var count = -1;

            while(CountBlackPixels(colors) != count)
            {
                count = CountBlackPixels(colors);
                var mask = 1 + 4 + 16 + 64;
                for (int x = 0; x < bitmap.PixelWidth; x++)
                {
                    for (int y = 0; y < bitmap.PixelHeight; y++)
                    {
                        if (colors[x, y] != 1)
                            continue;

                        var value = GetNeighboursValue(x, y, colors);

                        if (value == 255)
                            continue;

                        if (value < 255)
                            colors[x, y] = 2;

                        if ((value & mask) == mask)
                            colors[x, y] = 3;

                        if (_fours.Contains(value))
                            colors[x, y] = 4;
                    }
                }

                for (int x = 0; x < bitmap.PixelWidth; x++)
                {
                    for (int y = 0; y < bitmap.PixelHeight; y++)
                    {
                        if (colors[x, y] == 4)
                            colors[x, y] = 0;
                    }
                }

                for (int K = 2; K <= 3; K++)
                {
                    for (int x = 0; x < bitmap.PixelWidth; x++)
                    {
                        for (int y = 0; y < bitmap.PixelHeight; y++)
                        {
                            if (colors[x, y] == K)
                            {
                                var value = GetNeighboursValue(x, y, colors);
                                if (_deleteValues.Contains(value))
                                    colors[x, y] = 0;
                                else
                                    colors[x, y] = 1;
                            }
                        }
                    }
                }


                for (int x = 0; x < bitmap.PixelWidth; x++)
                {
                    for (int y = 0; y < bitmap.PixelHeight; y++)
                    {
                        if (colors[x, y] == 1)
                            SetPixel(x, y, new PixelColorInfo(0, 0, 0, 255));
                        else
                            SetPixel(x, y, new PixelColorInfo(255, 255, 255, 255));
                    }
                }
            }
           

            WritePixelsToBitmap();
        }

        private int CountBlackPixels(int[,] colors)
        {
            var count = 0;
            for(int x = 0; x <bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                    if (colors[x, y] == 1)
                        count++;
            }
            return count;
        }

        private void SetColors(int[,] colors)
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    if (GetPixel(x, y).R == 0)
                        colors[x, y] = 1;
                    else
                        colors[x, y] = 0;
                }
            }
        }

        private (double minR, double maxR, double minG, double maxG, double minB, double maxB) GetColorLimits()
        {
            double maxR = 0, maxG = 0, maxB = 0;
            double minR = 300, minG = 300, minB = 300;

            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    var color = GetPixel(x, y);
                    maxR = Math.Max(maxR, color.R);
                    minR = Math.Min(minR, color.R);
                    maxG = Math.Max(maxG, color.G);
                    minG = Math.Min(minG, color.G);
                    maxB = Math.Max(maxB, color.B);
                    minB = Math.Min(minB, color.B);
                }
            }
            return (minR, maxR, minG, maxG, minB, maxB);
        }


        private int GetNeighboursValue(int x, int y, int[,] colors)
        {
            int value = 0;
            int[,] mask = { { 128, 1, 2 }, { 64, 0, 4 }, { 32, 16, 8 } };
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (x + i - 1 < 0 || x + i - 1 >= bitmap.PixelWidth || y + j - 1 < 0 || y + j - 1 >= bitmap.PixelHeight)
                        continue;

                    if (colors[x + i - 1, y + j - 1] != 0)
                        value += mask[i, j];
                }
            }

            return value;
        }

        private int Truncate(double value)
        {
            if (value < 0)
                return 0;

            if (value > 255)
                return 255;

            return (int)Math.Round(value);
        }
    }
}
