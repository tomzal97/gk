﻿using GK2.Models;
using GK2.ViewModels;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace GK2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainViewModel viewModel;

        public MainWindow()
        {
            InitializeComponent();
            phong_disabled.IsChecked = true;
            viewModel = new MainViewModel(myCanvas);
            DataContext = viewModel;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void CheckIsNumber(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private static readonly Regex _numberRegex = new Regex("[^0-9.]+"); 
        private static bool IsTextAllowed(string text)
        {
            return !_numberRegex.IsMatch(text);
        }

        #region NotifyPropertyChangedImplementation

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

        private void RadiusTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void ObjectTexture_OnClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if(openFileDialog.ShowDialog() == true)
            {
                viewModel.ObjectTexture = new BitmapImage(new Uri(openFileDialog.FileName));
            }
        }

        private void NormalMap_OnClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                viewModel.NormalMap = new BitmapImage(new Uri(openFileDialog.FileName));
            }
        }

        private void HeightMap_OnClick(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                viewModel.HeightMap = new BitmapImage(new Uri(openFileDialog.FileName));
            }
        }
    }
}
