﻿using GK1.Controls;
using GK1.Models;
using GK2.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;

namespace GK2.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        private BitmapImage _objectTexture = new BitmapImage(new Uri(@"pack://application:,,,/Resources/polkadot.jpg"));

        public BitmapImage ObjectTexture
        {
            get => _objectTexture;
            set
            {
                SetProperty(ref _objectTexture, value);
                Canvas.ObjectTextureDrawer = new BitmapDrawer(ObjectTexture, (int)Canvas.canvas.ActualWidth, (int)Canvas.canvas.ActualHeight);
                Canvas.FillAllTriangles();
            }
        }

        private BitmapImage _normalMap = new BitmapImage(new Uri(@"pack://application:,,,/Resources/defaultNormalmap.png"));

        public BitmapImage NormalMap
        {
            get => _normalMap;
            set
            {
                SetProperty(ref _normalMap, value);
                Canvas.NormalMapDrawer = new BitmapDrawer(NormalMap, (int)Canvas.canvas.ActualWidth, (int)Canvas.canvas.ActualHeight);
                Canvas.FillAllTriangles();
            }
        }

        private BitmapImage _heightMap = new BitmapImage(new Uri(@"pack://application:,,,/Resources/defaultHeightmap.png"));

        public BitmapImage HeightMap
        {
            get => _heightMap;
            set
            {
                SetProperty(ref _heightMap, value);
                Canvas.HeightMapDrawer = new BitmapDrawer(HeightMap, (int)Canvas.canvas.ActualWidth, (int)Canvas.canvas.ActualHeight);
                Canvas.FillAllTriangles();
            }
        }

        private Color _selectedObjectColor = Colors.Red;

        public Color SelectedObjectColor
        {
            get => _selectedObjectColor;
            set
            {
                SetProperty(ref _selectedObjectColor, value);
                Canvas.FillAllTriangles();
            }
        }

        private Color _lightColor = Colors.White;

        public Color LightColor
        {
            get => _lightColor;
            set
            {
                SetProperty(ref _lightColor, value);
                Canvas.FillAllTriangles();
            }
        }

        private ObjectColorEnum _objectColorType = ObjectColorEnum.Texture;

        public ObjectColorEnum ObjectColorType
        {
            get => _objectColorType;
            set
            {
                SetProperty(ref _objectColorType, value);
                Canvas.FillAllTriangles();
            }
        }

        private NormalVectorEnum _normalVector = NormalVectorEnum.Texture;

        public NormalVectorEnum NormalVector
        {
            get => _normalVector;
            set
            {
                SetProperty(ref _normalVector, value);
                Canvas.FillAllTriangles();
            }
        }

        private DisturbedVectorEnum _disturbedVector = DisturbedVectorEnum.Texture;

        public DisturbedVectorEnum DisturbedVector
        {
            get => _disturbedVector;
            set
            {
                SetProperty(ref _disturbedVector, value);
                Canvas.FillAllTriangles();
            }
        }
        public Vector3D DefaultDisturbedVector { get; private set; } = new Vector3D(0, 0, 0);

        private LightVectorEnum _lightVector = LightVectorEnum.None;

        public LightVectorEnum LightVector
        {
            get => _lightVector;
            set
            {
                SetProperty(ref _lightVector, value);
                Canvas.FillAllTriangles();
            }
        }

        public Vector3D DefaultNormalVector { get; private set; } = new Vector3D(0, 0, 1);

        public string Radius { get; set; } = "100";

        public DrawingCanvas Canvas { get; set; }

        public bool PhongModel { get; set; } = false;

        public string PhongMParameter { get; set; } = "3";

        public MainViewModel(DrawingCanvas canv)
        {
            Canvas = canv;
        }

        #region NotifyPropertyChangedImplementation
        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion
    }
}
