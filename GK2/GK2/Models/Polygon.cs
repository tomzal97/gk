﻿using GK1.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Shapes;

namespace GK1.Models
{
    public class Polygon
    {
        public DrawingCanvas ParentCanvas { get; private set; }
        public List<Vertex> Vertices { get; set; } = new List<Vertex>();
        public List<LineControl> Lines { get; set; } = new List<LineControl>();

        public Polygon(DrawingCanvas parent)
        {
            ParentCanvas = parent;
        }

        public void AddVertex(Vertex vertex)
        {
            Vertices.Add(vertex);
        }

        public void MoveVertex(Vertex vertex)
        {
            foreach (var l in Lines)
                l.Refresh();
            ParentCanvas.FillAllTriangles();
        }

        internal void CreateLines()
        {
            Lines.Clear();
            for(int i=0; i<Vertices.Count; i++)
            {
                Lines.Add(new LineControl(this, Vertices[i], Vertices[(i + 1) % Vertices.Count]));
            }
        }
    }
}
