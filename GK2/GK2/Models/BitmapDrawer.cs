﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GK1.Models
{
    public class BitmapDrawer
    {
        public WriteableBitmap bitmap;

        private readonly int bytesPerPixel = 3;

        private byte[] _pixelsArray;

        private byte[] _whitePixelsArray;

        private int _stride;

        private double[,] _dhx;

        private double[,] _dhy;

        public BitmapDrawer(WriteableBitmap image)
        {
            SetupDrawer(image);
            SetBitmapWhite();
            Array.Copy(_pixelsArray, _whitePixelsArray, _pixelsArray.Length);
            WritePixelsToBitmap();
   
        }

        private void SetupDrawer(WriteableBitmap image)
        {
            bitmap = image;
            _stride = image.BackBufferStride;
            _pixelsArray = GetPixelsFromBitmap();
            _whitePixelsArray = new byte[_pixelsArray.Length];
            _dhx = new double[image.PixelWidth, image.PixelHeight];
            _dhy = new double[image.PixelWidth, image.PixelHeight];
            SetDH();
        }

        private void SetDH()
        {
            for(int i=0; i<bitmap.PixelWidth; i++)
            {
                for(int j=0; j<bitmap.PixelHeight; j++)
                {
                    var p = GetPixel(i, j);
                    var px = GetPixel(i + 1, j);
                    var py = GetPixel(i, j + 1);

                    _dhx[i, j] = (double)(px.R - p.R) / 255f;
                    _dhy[i, j] = (double)(py.R - p.R) / 255f;
                }
            }
        }

        public BitmapDrawer(BitmapImage bmp, int Width, int Height)
        {
            var w = bmp.PixelWidth > Width ? Width / bmp.Width : 1;
            var h = bmp.PixelHeight > Height ? Height / bmp.Height : 1;

            var scale = new ScaleTransform(w, h);

            var scaledBitmap = new TransformedBitmap(bmp, scale);

            FormatConvertedBitmap formatConverted = new FormatConvertedBitmap();

            formatConverted.BeginInit();
            formatConverted.Source = scaledBitmap ;
            formatConverted.DestinationFormat = PixelFormats.Rgb24;
            formatConverted.EndInit();

            SetupDrawer(new WriteableBitmap(formatConverted));
        }

        private byte[] GetPixelsFromBitmap()
        {
            var width = (int)bitmap.PixelWidth;
            var height = (int)bitmap.PixelHeight;

            int arraySize = _stride * height;
            byte[] pixels = new byte[arraySize];

            bitmap.CopyPixels(pixels, _stride, 0);

            return pixels;
        }

        public void WritePixelsToBitmap()
         {
            var width = (int)bitmap.PixelWidth;
            var height = (int)bitmap.PixelHeight;

            Int32Rect rect = new Int32Rect(0, 0, width, height);
            bitmap.WritePixels(rect, _pixelsArray, _stride, 0);
        }

        public (int R, int G, int B) GetPixel(int x, int y)
        {
            x = x % bitmap.PixelWidth;
            y = y % bitmap.PixelHeight;

            if (x < 0 || x >= bitmap.PixelWidth)
                return (-1, -1, -1);
            if (y >= bitmap.PixelHeight || y < 0)
                return (-1, -1, -1);

            var pos = x * bytesPerPixel + y * _stride;
            int R = _pixelsArray[pos];
            int G = _pixelsArray[pos + 1];
            int B = _pixelsArray[pos + 2];

            return (R, G, B);
        }

        public void SetPixel(int x, int y, (int R, int G, int B) colorInfo)
        {

            if (x >= bitmap.PixelWidth || y >= bitmap.PixelHeight || x < 0 || y < 0)
                return;

            var pos = x * bytesPerPixel + y * _stride;

            if (pos < 0 || pos >= _pixelsArray.Length)
                return;

            _pixelsArray[pos] = (byte)colorInfo.R;
            _pixelsArray[pos + 1] = (byte)colorInfo.G;
            _pixelsArray[pos + 2] = (byte)colorInfo.B;
        }

        private void SetBitmapWhite()
        {
            for (int x = 0; x < bitmap.PixelWidth; x++)
            {
                for (int y = 0; y < bitmap.PixelHeight; y++)
                {
                    SetPixel(x, y, (255, 255, 255));
                }
            }
        }

        public void Clear()
        {
            Array.Copy(_whitePixelsArray, _pixelsArray, _pixelsArray.Length);
            WritePixelsToBitmap();
        }
        
        public double GetDhx(int x, int y)
        {
            return _dhx[x % bitmap.PixelWidth, y % bitmap.PixelHeight];
        }

        public double GetDhy(int x, int y)
        {
            return _dhy[x % bitmap.PixelWidth, y % bitmap.PixelHeight];

        }

    }
}
