﻿using GK1.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK2.Models
{
    class AETNode
    {
        public double Ymax { get; set; }

        public double X { get; set; }

        public double Step { get; set; }

        public Vertex V1 { get; set; }

        public Vertex V2 { get; set; }

        public AETNode(Vertex v1, Vertex v2)
        {
            if(v1.Position.Y < v2.Position.Y)
            {
                Ymax = v2.Position.Y;
                X = v1.Position.X;
                V1 = v1;
                V2 = v2;
            }
            else
            {
                Ymax = v1.Position.Y;
                X = v2.Position.X;
                V1 = v2;
                V2 = v1;
            }

            double m;
            if (V1.Position.X == V2.Position.X)
                m = double.NaN;
            else
                m = (V2.Position.Y - V1.Position.Y) / (V2.Position.X - V1.Position.X);

            if (m == 0)
                m = V2.Position.X - V1.Position.X;

            Step = double.IsNaN(m) ? 0 : 1 / m;
        }
    }
}
