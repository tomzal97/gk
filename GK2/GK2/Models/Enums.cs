﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GK2.Models
{
    public enum ObjectColorEnum
    {
        Solid = 0,
        Texture
    }

    public enum NormalVectorEnum
    {
        Const = 0,
        Texture
    }
    
    public enum DisturbedVectorEnum
    {
        Const = 0,
        Texture
    }

    public enum LightVectorEnum
    {
        None = 0,
        Animated
    }
}
