﻿using GK2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace GK2.Converters
{
    public static class ColorVectorConverter
    {
        public static Vector3D ColorToNormalizedVector ((int R, int G, int B) color)
        {
            var X = (double)color.R / 255f;
            var Y = (double)color.G /255f;
            var Z = (double)color.B / 255f;

            return new Vector3D(X, Y, Z);
        }
        internal static Vector3D ColorToNormalizedVector(Color color)
        {
            return ColorToNormalizedVector((color.R, color.G, color.B));
        }

        internal static (int R, int G, int B) VectorTupleToColor(double X, double Y, double Z)
        {
            return ((int)(X * 255f), (int)(Y * 255f), (int)(Z *255f));
        }

        public static (int R, int G, int B) VectorToColor(Vector3D vector)
        {
            return VectorTupleToColor(vector.X, vector.Y, vector.Z);
        }

        internal static Vector3D ColorToNormalizedNormalMapVector((int R, int G, int B) color)
        {
            var X = (double)(color.R - 127f) / 128f;
            var Y = (double)(color.G - 127f) / 128f;
            var Z = (double)color.B / 255f;

            if (Z == 0)
            {
                X = 0;
                Y = 0;
                Z = 0;
            }
            else
            {
                X /= Z;
                Y /= Z;
                Z /= Z;
            }

            return new Vector3D(X, Y, Z);
        }
    }
}
