﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using GK1.Models;

namespace GK1.Controls
{
    /// <summary>
    /// Interaction logic for LineControl.xaml
    /// </summary>
    public partial class LineControl : UserControl, INotifyPropertyChanged
    {
        private Brush _color = Brushes.Black;

        public Brush LineColor
        {
            get => _color;
            set => SetProperty(ref _color, value);
        }

        public Vertex V1 { get; set; }

        public Vertex V2 { get; set; }

        public Models.Polygon ParentPolygon { get; set; }

        public LineControl(Models.Polygon parent)
        {
            InitializeComponent();
            DataContext = this;
            Panel.SetZIndex(this, 1);

            ParentPolygon = parent;

            OnPropertyChanged(nameof(LineColor));
        }

        public LineControl(Models.Polygon parent, Vertex v1, Vertex v2) : this(parent)
        {
            V1 = v1;
            V2 = v2;
        }

        public (double A, double B, double C) GetGeneralFormEquation()
        {
            var A = V1.Position.Y - V2.Position.Y;
            var B = V2.Position.X - V1.Position.X;
            var C = V1.Position.X * V2.Position.Y - V2.Position.X * V1.Position.Y;
            return (A, B, C);
        }

        public double GetSlope()
        {
        if (V1.Position.X == V2.Position.X)
            return double.NaN;
        return (V2.Position.Y - V1.Position.Y) / (V2.Position.X - V1.Position.X);
        }

        public void Refresh()
        {
            OnPropertyChanged(nameof(V1));
            OnPropertyChanged(nameof(V2));
        }

        #region NotifyPropertyChangedImplementation

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion
    }

}
