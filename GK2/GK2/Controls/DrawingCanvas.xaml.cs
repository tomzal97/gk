﻿using GK1.Models;
using GK2;
using GK2.Converters;
using GK2.Models;
using GK2.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace GK1.Controls
{
    /// <summary>
    /// Interaction logic for DrawingCanvas.xaml
    /// </summary>
    public partial class DrawingCanvas : UserControl, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private WriteableBitmap _bitmap;

        public WriteableBitmap CanvasBitmap
        {
            get => _bitmap;
            set => SetProperty(ref _bitmap, value);
        }

        public BitmapDrawer Drawer { get; private set; }

        public BitmapDrawer ObjectTextureDrawer { get; set; }

        public BitmapDrawer NormalMapDrawer { get; set; }

        public BitmapDrawer HeightMapDrawer { get; set; }

        public Models.Polygon Triangle1 { get; set; }

        public Models.Polygon Triangle2 { get; set; }

        private Color ObjectColor => parentViewModel.SelectedObjectColor;

        private (double R, double G, double B) LightColor => (parentViewModel.LightColor.R /255f, parentViewModel.LightColor.G /255f, parentViewModel.LightColor.G/255f);

        private string Radius => parentViewModel.Radius;

        private (double X, double Y) center;

        private MainViewModel parentViewModel;

        private double angle1 = 0, angle2 = 0, direction = 1;

        private BackgroundWorker worker;

        public DrawingCanvas()
        {
            InitializeComponent();
            DataContext = this;
            Dispatcher.BeginInvoke(DispatcherPriority.Loaded, new Action(() =>
            {
                PrepareWorker();

                var parentWindow = Window.GetWindow(this);
                parentViewModel = ((MainWindow)parentWindow).viewModel;
                CanvasBitmap = new WriteableBitmap((int)canvas.ActualWidth, (int)canvas.ActualHeight, 12, 12, PixelFormats.Rgb24, null);
                center = (CanvasBitmap.PixelWidth / 2, CanvasBitmap.PixelHeight / 2);
                Drawer = new BitmapDrawer(CanvasBitmap);
                ObjectTextureDrawer = new BitmapDrawer(parentViewModel.ObjectTexture, (int)canvas.ActualWidth, (int)canvas.ActualHeight);
                NormalMapDrawer = new BitmapDrawer(parentViewModel.NormalMap, (int)canvas.ActualWidth, (int)canvas.ActualHeight);
                HeightMapDrawer = new BitmapDrawer(parentViewModel.HeightMap, (int)canvas.ActualWidth, (int)canvas.ActualHeight);
                Drawer.Clear();
                CreateTriangles();
                worker.RunWorkerAsync();
            }));
        }

        private void PrepareWorker()
        {
            worker = new BackgroundWorker();
            worker.DoWork += new DoWorkEventHandler(worker_calculateAngles);
            worker.ProgressChanged += new ProgressChangedEventHandler(worker_ProgressChanged);
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
        }

        void worker_calculateAngles(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (worker.CancellationPending == true)
                {
                    e.Cancel = true;
                    return;
                }
                System.Threading.Thread.Sleep(150);

                angle2 = (angle2 + 2 * Math.PI / 100) % (2 * Math.PI);
                angle1 = angle1 + (Math.PI / 100) * direction;

                if (angle1 > Math.PI /2)
                {
                    angle1 = Math.PI /2;
                    direction = -1;
                }
                else if (angle1 < 0)
                {
                    angle1 = 0;
                    direction = 1;
                }
                (sender as BackgroundWorker).ReportProgress(1);
            }
        }

        void worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            FillAllTriangles();
        }


        public void DrawPolygons()
        {
            canvas.Children.Clear();
            DrawPolygon(Triangle1);
            DrawPolygon(Triangle2);
            FillAllTriangles();
        }

        public void DrawPolygon(Models.Polygon triangle)
        {
            foreach (var v in triangle.Vertices)
                DrawVertex(v);

            foreach (var l in triangle.Lines)
                DrawLine(l);
        }

        public void DrawVertex(Vertex vertex)
        {
            canvas.Children.Add(vertex);
            Canvas.SetLeft(vertex, vertex.Position.X - Vertex.Size / 2);
            Canvas.SetTop(vertex, vertex.Position.Y - Vertex.Size / 2);
        }

        public void DrawLine(LineControl line)
        {
            canvas.Children.Add(line);
        }

        private void CreateTriangles()
        {
            Triangle1 = new Models.Polygon(this);
            Triangle2 = new Models.Polygon(this);

            Triangle1.AddVertex(new Vertex(new Point(150, 200), Triangle1));
            Triangle1.AddVertex(new Vertex(new Point(270, 200), Triangle1));
            Triangle1.AddVertex(new Vertex(new Point(200, 375), Triangle1));

            Triangle1.CreateLines();

            Triangle2.AddVertex(new Vertex(new Point(325, 450), Triangle2));
            Triangle2.AddVertex(new Vertex(new Point(150, 550), Triangle2));
            Triangle2.AddVertex(new Vertex(new Point(325, 150), Triangle2));
            Triangle2.CreateLines();

            DrawPolygons();
        }

        public void FillAllTriangles()
        {
            Drawer.Clear();

            if(parentViewModel.LightVector == LightVectorEnum.None)
            {
                worker.CancelAsync();
            }
            else if(!worker.IsBusy)
            {
                worker.RunWorkerAsync();
            }

            FillTriangle(Triangle1);
            FillTriangle(Triangle2);
            Drawer.WritePixelsToBitmap();
        }

        private void FillTriangle(Models.Polygon triangle)
        {
            var ind = GetSortedIndices(triangle);

            int ymin = (int)triangle.Vertices[ind[0]].Position.Y;
            int ymax = (int)triangle.Vertices[ind[2]].Position.Y;

            List<AETNode> AET = new List<AETNode>();

            var currentInd = 0;
            for (int y = ymin + 1; y <= ymax; y++)
            {
                while (triangle.Vertices[ind[currentInd]].Position.Y == y - 1)
                {
                    var vPrev = triangle.Vertices[ind[(currentInd - 1 < 0 ? ind.Count - 1 : currentInd - 1)]];
                    var vNext = triangle.Vertices[ind[(currentInd + 1) % ind.Count]];
                    var v = triangle.Vertices[ind[currentInd]];

                    if (vPrev.Position.Y > v.Position.Y)
                        AET.Add(new AETNode(vPrev, v));
                    else
                        AET.Remove(AET.Where(x => x.V1 == vPrev && x.V2 == v).FirstOrDefault());

                    if (vNext.Position.Y > v.Position.Y)
                        AET.Add(new AETNode(vNext, v));
                    else
                        AET.Remove(AET.Where(x => x.V1 == vNext && x.V2 == v).FirstOrDefault());
                    currentInd = (currentInd + 1) % ind.Count;
                }

                UpdateAndDrawAET(AET, y);
            }
        }

        private void UpdateAndDrawAET(List<AETNode> AET, int y)
        {
            var lV = LightColor;
            double lx = 0, ly = 0, lz = 0;
            double m;
            double.TryParse(parentViewModel.PhongMParameter, out m);

            Vector3D N = parentViewModel.DefaultNormalVector;
            Vector3D D = parentViewModel.DefaultDisturbedVector;
            Vector3D T = new Vector3D(1, 0, 0);
            Vector3D B = new Vector3D(0, 1, 0);
            Vector3D L;
            if(parentViewModel.LightVector == LightVectorEnum.Animated)
            {
                double r;
                double.TryParse(Radius, out r);
                lx = r * Math.Cos(angle1) * Math.Sin(angle2) + center.X;
                ly = r * Math.Cos(angle2) * Math.Cos(angle1) + center.Y;
                lz = r * Math.Sin(angle1);
                L = new Vector3D(lx, ly, lz);
            }
            else
            {
                L = new Vector3D(0, 0, 1);
            }

            for (int i = 0; i < AET.Count; i += 2)
            {
                AET = AET.OrderBy(a => a.X).ToList();
                for (int x = (int)AET[i].X; x <= (int)AET[i + 1].X; x++)
                {
                    var c = parentViewModel.ObjectColorType == ObjectColorEnum.Texture ? ObjectTextureDrawer.GetPixel(x, y) : (ObjectColor.R, ObjectColor.G, ObjectColor.B);
                    if (c.R == -1)
                        continue;

                    N = parentViewModel.DefaultNormalVector;
                    if (parentViewModel.NormalVector == NormalVectorEnum.Texture)
                    {
                        N = ColorVectorConverter.ColorToNormalizedNormalMapVector(NormalMapDrawer.GetPixel(x, y));
                        T.Z = -N.Y;
                        B.Z = -N.X;
                    }
                    
                    if(parentViewModel.DisturbedVector == DisturbedVectorEnum.Texture)
                    {
                        var hx = HeightMapDrawer.GetDhx(x, y);
                        var hy = HeightMapDrawer.GetDhy(x, y);
                        D = T * hx + B * hy;
                    }

                    N = N + D;
                    N.Normalize();

                    if(parentViewModel.LightVector == LightVectorEnum.Animated)
                    {
                        L.X = lx - x;
                        L.Y = ly - y;
                        L.Z = lz;
                    }
                    L.Normalize();

                    var cos = L.X * N.X + L.Y * N.Y + L.Z * N.Z;
                    (int R, int G, int B) colorLambert = ((int)(c.R * cos * lV.R), (int)(c.G * cos * lV.G), (int)(c.B * cos * lV.B));

                    if(parentViewModel.PhongModel == true)
                    {
                        N = 2 * N - L;
                        N.Normalize();
                        var cosP = Math.Pow(N.Z, m);
                        (int R, int G, int B) colorPhong = ((int)(c.R * cosP * lV.R), (int)(c.G * cosP * lV.G), (int)(c.B * cosP * lV.B));
                        Drawer.SetPixel(x, y, ((colorLambert.R + colorPhong.R)/2, (colorLambert.G + colorPhong.G)/2, (colorLambert.B + colorPhong.B)/2));
                    }
                    else
                    {
                        Drawer.SetPixel(x, y, (colorLambert.R, colorLambert.G, colorLambert.B));
                    }
                }
                AET[i].X += AET[i].Step;
                AET[i + 1].X += AET[i + 1].Step;
            }
        }

        private static List<int> GetSortedIndices(Models.Polygon triangle)
        {
            var sorted = triangle.Vertices
                            .Select((x, i) => new KeyValuePair<double, int>(x.Position.Y, i))
                            .OrderBy(x => x.Key)
                            .ToList();

            List<int> ind = sorted.Select(x => x.Value).ToList();
            return ind;
        }

        #region NotifyPropertyChangedImplementation

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

    }
}
