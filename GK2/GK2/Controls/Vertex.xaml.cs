﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using GK1.Models;

namespace GK1.Controls
{
    /// <summary>
    /// Interaction logic for Vertex.xaml
    /// </summary>
    public partial class Vertex : UserControl, INotifyPropertyChanged
    {
        static public int Size { get; set; } = 15;

        private readonly Brush _inactiveColor = Brushes.Black;

        private readonly Brush _activeColor = Brushes.Red;

        public Brush Color
        {
            get => _color;
            set => SetProperty(ref _color, value);
        }

        private Brush _color;

        public Point Position { get; set; }


        public Polygon ParentPolygon { get; set; }

        private bool _isChoosed = false;

        public event PropertyChangedEventHandler PropertyChanged;

        public Vertex(Point pos, Models.Polygon parent)
        {
            InitializeComponent();
            DataContext = this;
            Position = pos;
            ParentPolygon = parent;
            _color = _inactiveColor;
            Panel.SetZIndex(this, 10);
        }

        private void ActivateVertex(object sender, MouseButtonEventArgs e)
        {
            _isChoosed = !_isChoosed;
            Color = _isChoosed ? _activeColor : _inactiveColor;
        }

        private void Thumb_DragDelta(object sender, System.Windows.Controls.Primitives.DragDeltaEventArgs e)
        {
            var left = Canvas.GetLeft(this) + e.HorizontalChange ;
            if (left < 0)
                left = 0;
            if (left > ParentPolygon.ParentCanvas.ActualWidth)
                left = ParentPolygon.ParentCanvas.ActualWidth;

            var top = Canvas.GetTop(this) + e.VerticalChange;
            if (top < 0)
                top = 0;
            if (top > ParentPolygon.ParentCanvas.ActualHeight)
                top = ParentPolygon.ParentCanvas.ActualHeight;

            Position = new Point(left, top);
            Canvas.SetLeft(this, left - Size/2);
            Canvas.SetTop(this, top - Size/2);

            Color = _activeColor;
            ParentPolygon.MoveVertex(this);
        }

        private void Thumb_DragCompleted(object sender, System.Windows.Controls.Primitives.DragCompletedEventArgs e)
        {
            Color = _inactiveColor;
        }


        #region NotifyPropertyChangedImplementation

        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        protected virtual bool SetProperty<T>(ref T storage, T value, [CallerMemberName] string propertyName = "")
        {
            if (EqualityComparer<T>.Default.Equals(storage, value))
                return false;
            storage = value;
            this.OnPropertyChanged(propertyName);
            return true;
        }

        #endregion

    }
}
